#!/usr/bin/env python
import argparse
import logging
import sys
from pathlib import Path

import yaml

log = logging.getLogger(__name__)


def extract_variables_from_profile_and_job(
    yaml_file: Path, profile_name: str, job_name: str
):
    with yaml_file.open("rb") as fd:
        data = yaml.load(fd, Loader=yaml.loader.SafeLoader)
    if (profile_dict := data.get("profiles", {}).get(profile_name)) is None:
        raise ValueError(f"Profile {profile_name!r} not found in {str(yaml_file)!r}")
    if (job_dict := profile_dict.get(job_name)) is None:
        raise ValueError(f"Job {job_name!r} not found in {str(yaml_file)!r}")

    return job_dict


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("parameters_file", type=Path, help="parameters YAML file")
    parser.add_argument("profile_name", type=str, help="profile name to extract")
    parser.add_argument(
        "job_name", type=str, help="job name, either 'download' or 'compute'"
    )
    args = parser.parse_args()

    if not args.parameters_file.is_file():
        parser.error("Can't find parameters YAML file")

    variables_dict = extract_variables_from_profile_and_job(
        args.parameters_file, args.profile_name, args.job_name
    )
    print(yaml.dump({"variables": variables_dict}, allow_unicode=True))


if __name__ == "__main__":
    sys.exit(main())

from pathlib import Path

import pytest
import yaml
from extract_env import extract_variables_from_profile_and_job


@pytest.fixture
def ab_parameters() -> Path:
    return Path("tests/fixtures/ab_profile_parameters.yaml")


def test_yaml_load(ab_parameters: Path):
    with ab_parameters.open("rb") as f:
        yaml.load(f, Loader=yaml.loader.SafeLoader)


def test_get_absent_profile(ab_parameters: Path):
    with pytest.raises(ValueError):
        extract_variables_from_profile_and_job(ab_parameters, "xx", "zz")


def test_get_absent_job(ab_parameters: Path):
    with pytest.raises(ValueError):
        extract_variables_from_profile_and_job(ab_parameters, "ab", "zz")


def test_get_existing_profile_and_job(ab_parameters: Path):
    extract_variables_from_profile_and_job(ab_parameters, "ab", "download")


def test_get_existing_profile_and_job_download(ab_parameters: Path):
    variables = extract_variables_from_profile_and_job(ab_parameters, "ab", "download")
    assert len(variables) == 2
    assert variables.keys() == {"d1", "d2"}


def test_get_existing_profile_and_job_convert(ab_parameters: Path):
    variables = extract_variables_from_profile_and_job(ab_parameters, "ab", "compute")
    assert len(variables) == 2
    assert variables.keys() == {"c1", "c2"}


def test_download_variables_type_and_values(ab_parameters: Path):
    variables = extract_variables_from_profile_and_job(ab_parameters, "ab", "download")
    assert variables["d1"] == "D1"
    assert variables["d2"] == 7


def test_compute_variables_type_and_values(ab_parameters: Path):
    variables = extract_variables_from_profile_and_job(ab_parameters, "ab", "compute")
    c1_var = variables["c1"]
    assert isinstance(c1_var, float)
    assert f"{c1_var:.1f}" == "3.6"
    assert variables["c2"] == "L'amour est enfant de bohême"

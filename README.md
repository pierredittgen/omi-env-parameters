# OMI env parameters

Extract profile/job environment variables from indicator builder parameters.yaml

## Install

Using a virtual env:

```bash
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
pip install -r requirements-dev.txt
```
